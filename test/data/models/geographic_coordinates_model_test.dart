/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:convert';

import 'package:clima/data/models/geographic_coordinates_model.dart';
import 'package:clima/domain/entities/city.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('GeographicCoordinatesModel', () {
    test('fromLocalJson', () {
      final json = jsonDecode('{"city":"Los Angeles","long":23,"lat":90}')
          as Map<String, dynamic>;

      expect(
        GeographicCoordinatesModel.fromLocalJson(json),
        const GeographicCoordinatesModel(
          city: City(name: 'Los Angeles'),
          long: 23,
          lat: 90,
        ),
      );
    });

    test('fromRemoteJson', () {
      final json = jsonDecode(
        // Geocoding data fetched from Open-Meteo (https://open-meteo.com/en/docs/geocoding-api),
        // based on data from GeoNames (https://www.geonames.org/) licensed
        // under the CC BY 4.0.
        '{"results":[{"id":108410,"name":"Riyadh","latitude":24.68773,"longitude":46.72185,"elevation":612.0,"feature_code":"PPLC","country_code":"SA","admin1_id":108411,"timezone":"Asia/Riyadh","population":4205961,"country_id":102358,"country":"Saudi Arabia","admin1":"Riyadh Region"},{"id":12324108,"name":"Riyadh","latitude":15.5834,"longitude":32.5677,"elevation":379.0,"feature_code":"PPLA3","country_code":"SD","admin1_id":379253,"timezone":"Africa/Khartoum","country_id":366755,"country":"Sudan","admin1":"Khartoum"},{"id":400679,"name":"King Salman Airbase","latitude":24.72045,"longitude":46.72065,"elevation":634.0,"feature_code":"AIRB","country_code":"SA","admin1_id":108411,"timezone":"Asia/Riyadh","country_id":102358,"country":"Saudi Arabia","admin1":"Riyadh Region"},{"id":6300026,"name":"Riyadh Air Base","latitude":24.70983,"longitude":46.72517,"elevation":634.0,"feature_code":"AIRP","country_code":"SA","timezone":"Asia/Riyadh","country_id":102358,"country":"Saudi Arabia"},{"id":400680,"name":"Riyadh Civil Defense Heliport","latitude":24.74708,"longitude":46.73693,"elevation":630.0,"feature_code":"AIRH","country_code":"SA","admin1_id":108411,"timezone":"Asia/Riyadh","country_id":102358,"country":"Saudi Arabia","admin1":"Riyadh Region"}],"generationtime_ms":0.28896332}',
      ) as Map<String, dynamic>;

      expect(
        GeographicCoordinatesModel.fromRemoteJson(json),
        const GeographicCoordinatesModel(
          city: City(name: 'Riyadh'),
          long: 46.72185,
          lat: 24.68773,
        ),
      );
    });

    test('toJson', () {
      expect(
        const GeographicCoordinatesModel(
          city: City(name: 'Manchester'),
          long: 35,
          lat: 25.4,
        ).toJson(),
        const <String, dynamic>{'city': 'Manchester', 'long': 35, 'lat': 25.4},
      );
    });
  });
}
