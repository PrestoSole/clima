# Clima ☁

**IMPORTANT**: Clima is looking for additional maintainers! Flutter experience in particular and mobile app development experience in general are preferred, but not required. Please send an e-mail to <lacerte@protonmail.com> if you're interested. Even if you're not willing to take on such a role, we would be glad if you'd help spread the word.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="75" alt="Get it on F-Droid">](https://f-droid.org/en/packages/co.prestosole.clima/)

Beautiful, minimal, and fast weather app. (Requires Android 6.0 or later)

<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/shot_01.png" alt="Beautiful, minimal UI" height="400" width="200"> <img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/shot_02.png" alt="8-day forecast" height="400" width="200"> <img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/shot_03.png" alt="Dark and light themes" height="400" width="200"> <img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/shot_04.png" alt="No ads or trackers" height="400" width="200">

## Features

- :white_check_mark: Beautiful, minimal UI
- :white_check_mark: 8-day forecast
- :white_check_mark: Imperial units support
- :white_check_mark: Dark and light themes
- :white_check_mark: No ads or trackers

## Donations

[<img src="repo_assets/bmc_button.svg" alt="Buy us a coffee" height="50">](https://www.buymeacoffee.com/yzooniee)

## Special Thanks

Special thanks to [Mohammed Anas](https://codeberg.com/triallax), without whom Clima would never be what it is today.

## Credits

* The app's weather data is provided by [Open-Meteo](https://open-meteo.com) under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Open-Meteo's data sources are documented [here](https://open-meteo.com/en/license).
* The app's geocoding data is provided by [Open-Meteo](https://open-meteo.com) under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/), based on data from [GeoNames](https://www.geonames.org/).
* The app logo's [icon](https://www.iconfinder.com/iconsets/tiny-weather-1) is designed by [Paolo Spot Valzania](https://linktr.ee/paolospotvalzania), licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) / Placed on top of a light blue background.
* The [weather icons](https://www.amcharts.com/free-animated-svg-weather-icons/) used inside the app (except for the "foggy" icon, see next entry) are designed by [amCharts](https://www.amcharts.com) and licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
* The ["foggy" icon](https://fonts.google.com/icons?selected=Material%20Symbols%20Outlined%3Afoggy%3AFILL%400%3Bwght%40400%3BGRAD%400%3Bopsz%4048) is a part of [Material Symbols](https://fonts.google.com/icons) by Google, licensed under the [Apache License, version 2.0](https://www.apache.org/licenses/LICENSE-2.0.html) / Icon color changed to light blue.

## Contact us

- Report a bug or request a feature: https://codeberg.org/Lacerte/clima/issues/new/choose (please choose the correct issue template and fill it out!)
- E-mail: <lacerte@protonmail.com>

## License

Clima is licensed under the Mozilla Public License, version 2.0. See [LICENSE](./LICENSE) for details.
