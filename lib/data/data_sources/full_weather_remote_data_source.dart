/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:convert';

import 'package:clima/core/either.dart';
import 'package:clima/core/failure.dart';
import 'package:clima/core/functions.dart';
import 'package:clima/data/models/full_weather_model.dart';
import 'package:clima/data/repos/geocoding_repo.dart';
import 'package:clima/domain/entities/city.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod/riverpod.dart';

class FullWeatherRemoteDataSource {
  FullWeatherRemoteDataSource(this._geocodingRepo);

  final GeocodingRepo _geocodingRepo;

  Future<Either<Failure, FullWeatherModel>> getFullWeather(City city) async {
    final coordinates =
        (await _geocodingRepo.getCoordinates(city)).fold((_) => null, id)!;

    final response = await http.get(
      Uri(
        scheme: 'https',
        host: 'api.open-meteo.com',
        path: 'v1/forecast',
        queryParameters: {
          'longitude': coordinates.long.toString(),
          'latitude': coordinates.lat.toString(),
          'current_weather': 'true',
          'timezone': 'auto',
          'timeformat': 'unixtime',
          'hourly':
              'temperature_2m,weathercode,precipitation_probability,weathercode,winddirection_10m,apparent_temperature,relativehumidity_2m,cloudcover,surface_pressure,is_day,uv_index',
          'daily':
              'weathercode,precipitation_probability_mean,temperature_2m_min,temperature_2m_max,sunrise,sunset',
        },
      ),
    );

    Object? body;

    try {
      body = jsonDecode(response.body);
    } on FormatException {
      return const Left(FailedToParseResponse());
    }

    if (body is! Map<String, dynamic>) {
      return const Left(FailedToParseResponse());
    }

    if (body['error'] == true) {
      return Left(ServerError(reason: body['reason'] as String?));
    } else {
      try {
        return Right(FullWeatherModel.fromJson(body, city: city));
      } catch (_) {
        return const Left(FailedToParseResponse());
      }
    }
  }
}

final fullWeatherRemoteDataSourceProvider = Provider(
  (ref) => FullWeatherRemoteDataSource(ref.watch(geocodingRepoProvider)),
);
