/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'dart:convert';

import 'package:clima/core/either.dart';
import 'package:clima/core/failure.dart';
import 'package:clima/data/models/geographic_coordinates_model.dart';
import 'package:clima/domain/entities/city.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod/riverpod.dart';

class GeocodingRemoteDataSource {
  Future<Either<Failure, GeographicCoordinatesModel>> getCoordinates(
    City city,
  ) async {
    final response = await http.get(
      Uri(
        scheme: 'https',
        host: 'geocoding-api.open-meteo.com',
        path: 'v1/search',
        queryParameters: {
          'name': city.name,
          'count': '1',
        },
      ),
    );

    Object? body;

    try {
      body = jsonDecode(response.body);
    } on FormatException {
      return const Left(FailedToParseResponse());
    }

    if (body is! Map<String, dynamic>) {
      return const Left(FailedToParseResponse());
    }

    if (body['error'] == true) {
      return Left(ServerError(reason: body['reason'] as String?));
    } else if (body['results'] == null) {
      // XXX: API returns 200 in this case, so we have to handle this separately.
      return Left(InvalidCityName(city.name));
    }

    try {
      return Right(GeographicCoordinatesModel.fromRemoteJson(body));
    } catch (_) {
      return const Left(FailedToParseResponse());
    }
  }
}

final geocodingRemoteDataSourceProvider =
    Provider((ref) => GeocodingRemoteDataSource());
