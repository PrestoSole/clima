/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// ignore_for_file: avoid_dynamic_calls

import 'package:clima/core/date_time.dart' as date_time_utils;
import 'package:clima/domain/entities/city.dart';
import 'package:clima/domain/entities/daily_forecast.dart';
import 'package:clima/domain/entities/full_weather.dart';
import 'package:clima/domain/entities/hourly_forecast.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/domain/entities/weather.dart';
import 'package:clima/domain/entities/wind_direction.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:timezone/timezone.dart' as tz;

class FullWeatherModel extends Equatable {
  const FullWeatherModel(this.fullWeather);

  factory FullWeatherModel.fromJson(
    Map<String, dynamic> json, {
    required City city,
  }) {
    final currentWeatherJson = json['current_weather'];

    final location = tz.getLocation(json['timezone'] as String);

    final currentDateTime = date_time_utils.fromUtcUnixTime(
      location,
      currentWeatherJson['time'] as int,
    );

    final hourlyJson = json['hourly'];
    final hourlyForecasts = List.generate(
      (hourlyJson['time'] as List).length - 1,
      (index) => HourlyForecast(
        unitSystem: UnitSystem.metric,
        date: date_time_utils.fromUtcUnixTime(
          location,
          hourlyJson['time'][index] as int,
        ),
        weatherCode: hourlyJson['weathercode'][index] as int,
        temperature: (hourlyJson['temperature_2m'][index] as num).toDouble(),
        // The probability of precipitation returned by Open-Meteo is for the
        // hour _before_ the forecast's indicated time. This is not how users
        // generally interpret probability of precipitation though, so here we
        // shift the index by 1 to make it so that the probability is for the
        // hour _after_ the current forecast's time.
        pop: (hourlyJson['precipitation_probability'][index + 1] as num)
            .toDouble(),
        isDay: () {
          switch (hourlyJson['is_day'][index]) {
            case 1:
              return true;

            case 0:
              return false;

            default:
              throw Error();
          }
        }(),
      ),
    );

    final closestHourlyForecastIndex =
        minBy<MapEntry<int, HourlyForecast>, Duration>(
      hourlyForecasts
          .asMap()
          .entries
          .where((entry) => entry.value.date.hour == currentDateTime.hour),
      (entry) => entry.value.date.difference(currentDateTime).abs(),
    )!
            .key;

    final dailyJson = json['daily'];
    final dailyForecasts = List.generate(
      (dailyJson['time'] as List).length,
      (index) => DailyForecast(
        unitSystem: UnitSystem.metric,
        maxTemperature:
            (dailyJson['temperature_2m_max'][index] as num).toDouble(),
        minTemperature:
            (dailyJson['temperature_2m_min'][index] as num).toDouble(),
        date: date_time_utils.fromUtcUnixTime(
          location,
          dailyJson['time'][index] as int,
        ),
        weatherCode: dailyJson['weathercode'][index] as int,
        pop: (dailyJson['precipitation_probability_mean'][index] as num)
            .toDouble(),
        sunrise: date_time_utils.fromUtcUnixTime(
          location,
          dailyJson['sunrise'][index] as int,
        ),
        sunset: date_time_utils.fromUtcUnixTime(
          location,
          dailyJson['sunset'][index] as int,
        ),
      ),
    );

    return FullWeatherModel(
      FullWeather(
        city: city,
        unitSystem: UnitSystem.metric,
        currentWeather: Weather(
          unitSystem: UnitSystem.metric,
          temperature: (currentWeatherJson['temperature'] as num).toDouble(),
          tempFeel: (hourlyJson['apparent_temperature']
                  [closestHourlyForecastIndex] as num)
              .toDouble(),
          windSpeed: (currentWeatherJson['windspeed'] as num).toDouble(),
          windDirection: () {
            final windDegree = (hourlyJson['winddirection_10m']
                    [closestHourlyForecastIndex] as num)
                .toDouble();

            // The wind degree is measured clockwise from north.
            //
            // If a degree happens to be right in the middle between two
            // cardinal directions, we have to choose one of the directions. In
            // that case, here we opt for the "main" direction of the two, i.e.
            // north, south, east, or west.
            if (windDegree >= 337.5 || windDegree <= 22.5) {
              return WindDirection.north;
            } else if (windDegree > 292.5) {
              return WindDirection.northwest;
            } else if (windDegree >= 247.5) {
              return WindDirection.west;
            } else if (windDegree > 202.5) {
              return WindDirection.southwest;
            } else if (windDegree >= 157.5) {
              return WindDirection.south;
            } else if (windDegree > 112.5) {
              return WindDirection.southeast;
            } else if (windDegree >= 67.5) {
              return WindDirection.east;
            } else if (windDegree > 22.5) {
              return WindDirection.northeast;
            }

            // This should never happen, assuming the above code is correct at
            // least.
            throw Error();
          }(),
          date: date_time_utils.fromUtcUnixTime(
            location,
            currentWeatherJson['time'] as int,
          ),
          weatherCode: currentWeatherJson['weathercode'] as int,
          humidity: hourlyJson['relativehumidity_2m']
              [closestHourlyForecastIndex] as int,
          clouds: hourlyJson['cloudcover'][closestHourlyForecastIndex] as int,
          pressure: (hourlyJson['surface_pressure'][closestHourlyForecastIndex]
                  as num)
              .round(),
          uvIndex: (hourlyJson['uv_index'][closestHourlyForecastIndex] as num)
              .toDouble(),
        ),
        dailyForecasts: dailyForecasts,
        hourlyForecasts: hourlyForecasts
            .skipWhile(
              (forecast) =>
                  forecast.date.difference(currentDateTime) >
                      const Duration(hours: 1) ||
                  forecast.date.hour != currentDateTime.hour,
            )
            .toList(),
      ),
    );
  }

  final FullWeather fullWeather;

  @override
  List<Object?> get props => [fullWeather];
}
