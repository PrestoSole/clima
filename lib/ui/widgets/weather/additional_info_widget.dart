/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/domain/entities/wind_direction.dart';
import 'package:clima/ui/state_notifiers/full_weather_state_notifier.dart' as w;
import 'package:clima/ui/state_notifiers/unit_system_state_notifier.dart';
import 'package:clima/ui/widgets/weather/additional_info_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';

class AdditionalInfoWidget extends ConsumerWidget {
  const AdditionalInfoWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final currentWeather = ref.watch(
      w.fullWeatherStateNotifierProvider.select(
        (state) => state.fullWeather!.currentWeather,
      ),
    );
    final currentDayForecast = ref.watch(
      w.fullWeatherStateNotifierProvider.select(
        (state) => state.fullWeather!.currentDayForecast,
      ),
    );

    final unitSystem = ref.watch(
      unitSystemStateNotifierProvider.select(
        (state) => state.unitSystem!,
      ),
    );

    final timeFormat = MediaQuery.of(context).alwaysUse24HourFormat
        ? DateFormat.Hm()
        : DateFormat('h:mm a');

    final String windDirectionString;

    switch (currentWeather.windDirection) {
      case WindDirection.north:
        windDirectionString = '↑';
        break;

      case WindDirection.northeast:
        windDirectionString = '↖';
        break;

      case WindDirection.east:
        windDirectionString = '←';
        break;

      case WindDirection.southeast:
        windDirectionString = '↙';
        break;

      case WindDirection.south:
        windDirectionString = '↓';
        break;

      case WindDirection.southwest:
        windDirectionString = '↘';
        break;

      case WindDirection.west:
        windDirectionString = '→';
        break;

      case WindDirection.northwest:
        windDirectionString = '↗';
        break;
    }

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
          child: Row(
            children: [
              AdditionalInfoTile(
                title: 'Feels like',
                value: '${currentWeather.tempFeel.round()}°',
              ),
              AdditionalInfoTile(
                title: 'Humidity',
                value: '${currentWeather.humidity}%',
              ),
              AdditionalInfoTile(
                title: 'Wind speed',
                value: '${currentWeather.windSpeed.round()} ${() {
                  switch (unitSystem) {
                    case UnitSystem.metric:
                      return 'km/h';

                    case UnitSystem.imperial:
                      return 'mph';
                  }
                }()} $windDirectionString',
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
          child: Row(
            children: [
              AdditionalInfoTile(
                title: 'Clouds',
                value: '${currentWeather.clouds}%',
              ),
              AdditionalInfoTile(
                title: 'UV index',
                value: currentWeather.uvIndex.toString(),
              ),
              AdditionalInfoTile(
                title: 'Chance of rain',
                value: '${currentDayForecast.pop.round()}%',
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              AdditionalInfoTile(
                title: 'Sunrise',
                value: timeFormat.format(currentDayForecast.sunrise),
              ),
              AdditionalInfoTile(
                title: 'Sunset',
                value: timeFormat.format(currentDayForecast.sunset),
              ),
              AdditionalInfoTile(
                title: 'Pressure',
                value: '${currentWeather.pressure} mbar',
              ),
            ],
          ),
        ),
      ],
    );
  }
}
