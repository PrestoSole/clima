/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/ui/widgets/settings/settings_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpAndFeedbackDialog extends StatelessWidget {
  const HelpAndFeedbackDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context)!;

    return SimpleDialog(
      title: Text(
        appLocalizations.helpAndFeedback_title,
        style: TextStyle(
          color: Theme.of(context).textTheme.titleMedium!.color,
        ),
      ),
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SettingsTile(
              title: appLocalizations.helpAndFeedback_openAnIssue,
              leading: const Icon(Icons.quiz_outlined),
              onTap: () => launchUrl(
                Uri.parse('https://codeberg.org/Lacerte/clima/issues/new'),
              ),
            ),
            SettingsTile(
              title: appLocalizations.helpAndFeedback_contactDeveloper,
              leading: Icon(
                Icons.email_outlined,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () => launchUrl(
                Uri.parse('mailto:lacerte@protonmail.com'),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
