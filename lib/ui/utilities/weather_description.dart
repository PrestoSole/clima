/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

String getWeatherDescription(int weatherCode) {
  // See table at bottom of https://open-meteo.com/en/docs.
  switch (weatherCode) {
    case 0:
      return 'Clear sky';

    case 1:
      return 'Mainly clear';

    case 2:
      return 'Partly cloudy';

    case 3:
      return 'Overcast';

    case 45:
      return 'Fog';

    case 48:
      return 'Depositing rime fog';

    case 51:
      return 'Light drizzle';

    case 53:
      return 'Moderate drizzle';

    case 55:
      return 'Dense drizzle';

    case 56:
      return 'Light freezing drizzle';

    case 57:
      return 'Dense freezing drizzle';

    case 61:
      return 'Light rain';

    case 63:
      return 'Moderate rain';

    case 65:
      return 'Heavy rain';

    case 66:
      return 'Light freezing rain';

    case 67:
      return 'Heavy freezing rain';

    case 71:
      return 'Light snow fall';

    case 73:
      return 'Moderate snow fall';

    case 75:
      return 'Heavy snow fall';

    case 77:
      return 'Snow grains';

    case 80:
      return 'Light rain showers';

    case 81:
      return 'Moderate rain showers';

    case 82:
      return 'Violent rain showers';

    case 85:
      return 'Light snow showers';

    case 86:
      return 'Heavy snow showers';

    case 95:
      return 'Light or moderate thunderstorm';

    case 96:
      return 'Thunderstorm with slight hail';

    case 99:
      return 'Thunderstorm with heavy hail';

    default:
      throw ArgumentError(
        'Failed to map weather code $weatherCode to description',
      );
  }
}
