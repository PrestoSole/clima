---
name: Feature request
about: Suggest an idea for this project
labels: [enhancement]
---

<!--
Clima is looking for additional maintainers! Flutter experience in particular and mobile app development experience in general are preferred, but not required. Please send an e-mail to lacerte@protonmail.com if you're interested. Even if you're not willing to take on such a role, we would be glad if you'd help spread the word.
-->

**Is your feature request related to a problem? Please describe.**
A clear and concise description of what the problem is, e.g. I'm always frustrated when [...]

**Describe the solution you'd like**
A clear and concise description of what you want to happen.

**Describe alternatives you've considered** (optional)
A clear and concise description of any alternative solutions or features you've considered.

**Additional context** (optional)
Add any other context or screenshots about the feature request here.
